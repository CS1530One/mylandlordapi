// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var tediousExpress = require('express4-tedious');
var tediousConnection = require('tedious').Connection;
var tediousRequest = require('tedious').Request;

var connection = {
    "server"  : "my-landlord.database.windows.net",
    "userName": "admin1",
    "password": "Password123",
    "options": { "encrypt": true, "database": "my-landlord-db" }
};

app.use(function (req, res, next) {

    req.sql = tediousExpress(connection);

    next();

});


var ted_conn = new tediousConnection(connection);


function queryDatabase(query, callback){
    var ted_conn = new tediousConnection(connection);
    var newdata = [];
    var dataset = [];
    ted_conn.on('connect', function(err) {

        var sql = query;

        var Request = tediousRequest;
        var request = new Request(sql, function (err, rowCount) {
            if (err) {
                callback(err);
            } else {
                if (rowCount < 1) {
                    callback(null, false);
                } else {
                    callback(null, newdata);
                }
            }
        });

        request.on('row', function(columns) {

            columns.forEach(function(column) {
                   dataset.push({
                       col: column.metadata.colName,
                       val: column.value
                   });


            });

            newdata.push(dataset);

        });

        ted_conn.execSql(request);

    });

}


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 1337;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api! You sent a GET!' });   
});
router.post('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api! You sent a POST!' });
    console.log(req.body.Username)
    console.log(req.body.Email)
    console.log(req.body.Password)

});
router.get('/tenants', function (req, res) {

    req.sql("select * from dbo.tenant for json path")
        .into(res);

});

router.post('/login', function (req, res) {

    uname = req.body.username;
    pwrd = req.body.password;
    uname_match = false;
    pwrd_match = false;
    str = 'select * from dbo.tenant';

    queryDatabase(str, function(err, rows) {
        if (err) {
            // Handle the error
        } else if (rows) {
            // Process the rows returned from the database
            rows.forEach(function(element) {
                element.forEach(function(el) {
                    if(el['col'] == 'username'){
                        if(el['val'].trim() == uname){
                            uname_match = true;
                        }
                    }
                    if(el['col'] == 'password'){
                        if(el['val'].trim() == pwrd){
                            pwrd_match = true;
                        }
                    }
                });
            });
            if(uname_match&&pwrd_match){
                //res.send('You successfully logged in!');
                res.send(200);
            }else{
                //res.send('Invalid credentials!');
                res.send(404);
            }
            
        } else {
            // No rows returns; handle appropriately
        }
    });
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
